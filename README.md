# Towards Dynamic Resource Management with MPI Sessions and PMIx - Prototype Code
## Introduction
This repository provides the code of the Dynamic MPI Processes prototype developed in the context of the EuroHPC Time-X project. It consists of:
* an ompi fork
* an openpmix fork
* a prrte fork
* a repo with test applications
* a repo with a (python-based) dynamic resource manager

More info on each of these projects will be provided soon ...

## Prerequisits
Open-MPI:
* m4 1.4.17
* autoconf 2.69
* automake 1.15
* libtool 2.4.6
* flex 2.5.35
* hwloc 2.5
* libevent 2.1.12
* zlib (recommended)
* python3
* cython

Building the benchmarks:
* scons 

## Compiling and Installing
### Docker Environment
For now, we only support the setup from within the docker environment.\
Support for setup in native environments will be supported soon.

The docker project can be retrieved here:\
`https://gitlab.inria.fr/dynres/dyn-procs/docker-cluster`

Instructions on how to setup the docker environment can be found in the corresponding README file.\
This README also contains all the instructions below.

Once the docker environment is set up, this respository (dyn_procs_setup) should be located in the '/build' directory of the docker project.  
 
### Environment variables
Before installing the projects, we need to export the required environment variables:\
`source env_vars_docker.sh`

### Building the projects
There's a special compile script automizing many steps of the installation.
To build all projects run:\
`./install_docker.sh all`

It is also possible to build the projects individually:

`./install_docker.sh openpmix` (Builds the OpenPMIx project)\
`./install_docker.sh prrte` (Builds the PRRTE project)\
`./install_docker.sh ompi` (Builds the OMPI project)\
`./install_docker.sh tests` (Builds the test applications)\
`./install_docker.sh dyn_rm` (Builds the Dynamic Resource Manager project)

Note the dependecies chain: tests -> ompi -> prrte -> openpmix

## Running test applications
There are test applications for the different versions of the dynamic MPI process interface in the "test_applications" directory.
A script to run all test cases is provided:\
`./dyn_mpi_sessions_test_all.sh` (4 nodes)\
`./dyn_mpi_sessions_test_all_8_nodes.sh` (8 nodes)

## Running the Dynamic Resource Manager
To run the dynamic resource manager go to the "dyn_rm" directory.

The command to run jobs with the dynamic resource manager is:\
`python3 dynamic_resource_manager.py --jobs=[path_to_job_mix_file] --host=[host_names] (optional: --verbosity=[1,4] (default: 1)) (optional: --sched_interval=[float] (default: 1))`

There are example job mix files provided in the examples subdirectory.\
The command to run the resource manager on a system with 4 nodes á 8 slots and the job mix file "job_mix_alternate.txt" looks as follows:\ 

`python3 dynamic_resource_manager.py --jobs=examples/job_mix_alternate.txt --host=n01:8,n02:8,n03:8,n04:8` 

## Contact
In case of any questions please contact me via email: `domi.huber@tum.de`


